var React = require('react');
var _ = require('lodash');
var classnames = require('classnames');

require('../../../../less/movie.less');

var ModuleStore = require('../../../stores/ModuleStore');
var ModuleActions = require('../../../actions/ModuleActions');
var Container = require('../common/ContainerModule.react');
var SearchBar = require('../common/SearchBarModule.react');
var NavBar = require('../common/NavBarModule.react');
var Player = require('../common/Player.react');

var Glyphicon = require('react-bootstrap/lib/Glyphicon');

var movies = [
    {
        id: 1,
        title: '速度与激情',
        actors: ['憨大叔', '萌萝莉'],
        rate: 7.3,
    },
    {
        id: 2,
        title: '哆啦A梦：伴我同行',
        actors: ['水田山葵', '大原惠美', '关智一'],
        rate: 8.1,
    },
    {
        id: 3,
        title: 'BlaBla小魔仙',
        actors: ['憨大叔', '萌萝莉'],
        rate: 7.3,
    },
    {
        id: 4,
        title: 'BlaBla小魔仙',
        actors: ['憨大叔', '萌萝莉'],
        rate: 7.3,
    },
    {
        id: 5,
        title: '哆啦A梦：伴我同行',
        actors: ['水田山葵', '大原惠美', '关智一'],
        rate: 8.1,
    },
    {
        id: 6,
        title: '哆啦A梦：伴我同行',
        actors: ['水田山葵', '大原惠美', '关智一'],
        rate: 8.1,
    },
    {
        id: 7,
        title: '哆啦A梦：伴我同行',
        actors: ['水田山葵', '大原惠美', '关智一'],
        rate: 8.1,
    },
    {
        id: 8,
        title: '哆啦A梦：伴我同行',
        actors: ['水田山葵', '大原惠美', '关智一'],
        rate: 8.1,
    },
    {
        id: 9,
        title: '哆啦A梦：伴我同行',
        actors: ['水田山葵', '大原惠美', '关智一'],
        rate: 8.1,
    },
];

var MovieItem = React.createClass({
    render: function () {
        var stuffix = this.props.id % 2 === 0 ? '1' : '2';
        var coverUrl = 'url(/movies/cover_' + stuffix + '.jpg)';
        var coverStl = {
            backgroundImage: coverUrl
        };
        var actors = _.chain(this.props.actors).map(function (actor, key) {
            return key === 0 ? <span key={key}><a>{actor}</a></span> : <span key={key}>/<a>{actor}</a></span>;
        }).value();
        return (
            <div className='movie-item-wrap'>
                <div className='cover' style={coverStl}>
                    <div className='widget'>
                        <div className='play-btn text-center'><Glyphicon glyph='play' onClick={this.props.showPlayer} /></div>
                        <div className='love-btn'></div>
                        <div className='share-btn'></div>
                    </div>
                    <div className='brand'>{this.props.rate}</div>
                </div>
                <h3 className='title'>{this.props.title}</h3>
                <p className='desc-text'>{actors}</p>
            </div>
        );
    }
});

var Movies = React.createClass({
    getInitialState: function () {
        return {
            movies: movies
        };
    },
    showPlayer: function () {
        this.refs.player.getDOMNode().style.display = 'block';
    },
    hidePlayer: function () {
        this.refs.player.getDOMNode().style.display = 'none';
    },
    render: function(){
        var that = this;
        var Movies = _.chain(this.state.movies).map(function (movie) {
            return (
                <MovieItem showPlayer = {that.showPlayer} key={movie.id} id={movie.id} title={movie.title} actors={movie.actors} rate={movie.rate}></MovieItem>
            );
        }).value();
        var videoOptions = {
            url: 'http://7xjjuu.dl1.z0.glb.clouddn.com/10852_0.mp4'
            //url: 'http://videos.thisisepic.com/2b9c1bf3-e19b-4be5-9d36-246c5d3607d8/high.mp4',
            //poster: 'http://thumbnails.thisisepic.com/b1ce00de-e687-4c1b-97ac-afa05a287327/large/frame_0005.png'
        };
        return (
            <Container>
                <SearchBar/>
                <div className="movies-container">
                    <NavBar/>
                    {Movies}
                    <Player ref = 'player' options = {videoOptions} hidePlayer = {this.hidePlayer}></Player>
                </div>
            </Container>
        );
    }
});

module.exports = Movies;